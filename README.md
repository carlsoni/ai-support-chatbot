AI Search Proof of Concept 
---

See: [Documentation](docs/index.md)

---

*This project use the Documentation as Code (Docs as Code) approach.
The idea is the technical documentation is versioned and stored alongside the code.
The technical documentation is also treated like code and committed the repository commit message
format requirements, like requiring issue number.*

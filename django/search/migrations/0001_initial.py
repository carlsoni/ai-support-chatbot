# Generated by Django 5.0.2 on 2024-02-25 04:11

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Knowledge',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('node_id', models.IntegerField()),
                ('title', models.CharField(max_length=200)),
                ('url', models.CharField(max_length=200)),
                ('content', models.TextField()),
            ],
        ),
    ]

from django.contrib import admin

# Register your models here.
from .models import Knowledge

admin.site.register(Knowledge)

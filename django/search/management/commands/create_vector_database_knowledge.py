from django.core.management.base import BaseCommand
from ...utils.vector_database import get_vector_database


class Command(BaseCommand):
    help = 'Create Vector Database collection for Knowledge'

    def handle(self, *args, **options):
        vector_db = get_vector_database()
        vector_db.create_collection("Knowledge")
        self.stdout.write(
            self.style.SUCCESS("Vector Database Collection 'Knowledge' "
                               "created successfully."))

        vector_db.disconnect()

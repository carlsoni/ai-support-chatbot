from django.core.management.base import BaseCommand
from ...utils.relational_database import get_relational_database
from ...utils.vector_creator import get_vector_creator
from ...utils.vector_database import get_vector_database


class Command(BaseCommand):
    help = 'Populates the vector database'

    def handle(self, *args, **options):
        vector_creator = get_vector_creator()
        self.stdout.write(
            self.style.SUCCESS("Vector Creator loaaded."))

        vector_database = get_vector_database()
        self.stdout.write(
            self.style.SUCCESS("Vector Database connected."))

        relation_database = get_relational_database()
        self.stdout.write(
            self.style.SUCCESS("Relational Database connected."))

        knowledge = relation_database.get_knowledge()
        self.stdout.write(
            self.style.SUCCESS("Got knowledge from relational database."))
        try:
            vector_database.save_knowledge(knowledge)
            self.stdout.write(
                self.style.SUCCESS("Knowledge saved to vector database."))
        except Exception as e:
            self.stdout.write(self.style.ERROR(e))

        relation_database.disconnect()
        vector_database.disconnect()

        self.stdout.write(
            self.style.SUCCESS('Successfully populated vector database'))

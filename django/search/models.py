from django.db import models


# Create your models here.
class Knowledge(models.Model):
    node_id = models.IntegerField()
    title = models.CharField(max_length=200)
    url = models.CharField(max_length=200)
    content = models.TextField()

    def __str__(self):
        return self.title

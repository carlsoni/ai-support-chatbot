from abc import ABC, abstractmethod
from angle_emb import AnglE, Prompts


class VectorCreator(ABC):
    @abstractmethod
    def create_vector(self, text):
        pass


# see: https://huggingface.co/WhereIsAI/UAE-Large-V1
class AnglEVectorCreator(VectorCreator):
    def __init__(self):
        # Initialize the AnglE model
        self.angle = AnglE.from_pretrained(
            'WhereIsAI/UAE-Large-V1',
            pooling_strategy='cls'
        ).cuda()
        self.angle.set_prompt(prompt=Prompts.C)

    def create_vector(self, text):
        # Flatten the list of lists to a single list
        return [item for sublist in
                self.angle.encode({'text': text}, to_numpy=True).tolist() for
                item in sublist]


# Factory method to get VectorCreator
def get_vector_creator() -> VectorCreator:
    return AnglEVectorCreator()

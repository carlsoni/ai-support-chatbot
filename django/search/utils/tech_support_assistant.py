from openai import OpenAI
from dotenv import load_dotenv
import os
from .vector_database import get_vector_database


class TechSupportAssistant:
    def __init__(self):
        load_dotenv()
        self.ai_client = OpenAI(
            api_key=os.environ.get('OPENAI_API_KEY'),
        )

    @staticmethod
    def fetch_knowledge_base_content(query):
        vector_database = get_vector_database()
        response = vector_database.search(query)
        results = []
        matched_content = ""
        for o in response.objects:
            results.append({
                'node_id': o.properties['node_id'],
                'title': o.properties['title'],
                'url': o.properties['url'],
                'distance': round(o.metadata.distance, 2),
            })
            matched_content += (f"Title: {o.properties['title']}\n"
                                f"URL:  {o.properties['url']}\n"
                                f"Article: {o.properties['content']}\n\n")
        vector_database.disconnect()
        return matched_content

    def generate_response(self, question):
        knowledge_base_content = self.fetch_knowledge_base_content(question)
        try:
            chat_completion = self.ai_client.chat.completions.create(
                model="gpt-3.5-turbo",
                messages=self.__get_prompt(knowledge_base_content, question),
                temperature=0,
                max_tokens=150,
                top_p=1,
                frequency_penalty=0,
                presence_penalty=0,
                stop=None,
            )
            first_choice = chat_completion.choices[0]
            print(first_choice.message)
            return first_choice.message.content
        except Exception as e:
            return f"Error in generating response: {str(e)}"

    # TODO: get help prompt engineering
    @staticmethod
    def __get_prompt(context, question):
        return [
            {
                'role': 'system',
                'content': f"You are a AI Tech Support Assistant at "
                           f"Sonoma State University. "
                           f"Answer the question based on the context below, "
                           f"and if the question can't be answered based on "
                           f"the context, say \"I don't know\" and provide "
                           f"links to the context articles as well as "
                           f"https://help.sonoma.edu/support/tickets/new to "
                           f"open a ticket. "
                           f"Send output in HTML\n\n"
            },
            {
                'role': 'user',
                'content': f"Context: {context}\n\n---\n\nQuestion: {question}\nAnswer:"
            }
        ]

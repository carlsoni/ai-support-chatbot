from abc import ABC, abstractmethod
import html
import re
import mysql.connector


class RelationalDatabase(ABC):
    @abstractmethod
    def connect(self, **kwargs):
        pass

    @abstractmethod
    def disconnect(self):
        pass

    @abstractmethod
    def get_knowledge(self) -> list:
        pass


class MariadbDatabaser(RelationalDatabase):
    def __init__(self):
        self.cnx = None
        self.cursor = None

    def connect(self):
        self.cnx = mysql.connector.connect(
            host='mariadb',
            user='root',
            password='root',
            database='drupal'
        )
        self.cursor = self.cnx.cursor()
        return self.cursor

    def disconnect(self):
        if self.cnx is not None:
            self.cnx.close()

    def get_rows(self, query):
        if self.cursor is None:
            self.connect()
        return self.cursor.execute(query)

    # Get Drupal knowledgebase data in expected format
    def get_knowledge(self) -> list:
        if self.cursor is None:
            self.connect()
        knowledge = list()
        query = (
            "SELECT n.nid, n.title, b.body_value " +
            "FROM node n " +
            "JOIN field_data_body b ON n.nid = b.entity_id " +
            "WHERE n.type = 'knowledgebase' AND n.status = 1"
        )
        self.cursor.execute(query)
        rows = self.cursor.fetchall()
        for row in rows:
            knowledge_obj = self.__row_to_knowledge_obj(row)
            knowledge.append(knowledge_obj)
        return knowledge

    @staticmethod
    def __row_to_knowledge_obj(row) -> dict:
        content = html.unescape(row[2])
        content = re.sub('<[^<]+?>', '', content)
        knowledge_obj = {
            "node_id": row[0],
            "title": row[1],
            "url": f"https://it.sonoma.edu/node/{row[0]}",
            "content": content
        }
        return knowledge_obj


# Factory method to get RelationalDatabase
def get_relational_database() -> RelationalDatabase:
    return MariadbDatabaser()

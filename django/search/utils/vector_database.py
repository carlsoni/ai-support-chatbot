from abc import ABC, abstractmethod
import weaviate
import weaviate.classes.query as wq
import weaviate.classes.config as wc
from weaviate.util import generate_uuid5
from .vector_creator import get_vector_creator


# Abstract class for VectorDatabase
class VectorDatabase(ABC):
    @abstractmethod
    def connect(self, **kwargs):
        pass

    @abstractmethod
    def create_collection(self, collection_name: str):
        pass

    @abstractmethod
    def delete_collection(self, collection_name: str):
        pass

    @abstractmethod
    def get_collection(self, collection_name: str):
        pass

    @abstractmethod
    def save_knowledge(self, knowledge_list: list):
        pass

    @abstractmethod
    def search(self, query: str):
        pass

    @abstractmethod
    def disconnect(self):
        pass


# Weaviate implementation of VectorDatabase
class WeaviateVectorDatabase(VectorDatabase):
    def __init__(self):
        self.client = None

    def connect(self):
        self.client = weaviate.connect_to_custom(
            http_host="weaviate",
            http_port=8080,
            http_secure=False,
            grpc_host="weaviate",
            grpc_port=50051,
            grpc_secure=False,
        )
        return self.client

    def create_collection(self, collection_name: str):
        if self.client is None:
            self.connect()
        self.client.collections.create(
            name="Knowledge",
            properties=[
                wc.Property(name="node_id", data_type=wc.DataType.INT),
                wc.Property(name="title", data_type=wc.DataType.TEXT),
                wc.Property(name="url", data_type=wc.DataType.TEXT),
                wc.Property(name="content", data_type=wc.DataType.TEXT),
            ],
            # Define the vectorizer module (none, using our own vectors)
            vectorizer_config=wc.Configure.Vectorizer.none(),
            # Define the generative module
            # generative_config=wc.Configure.Generative.none()
        )

    def delete_collection(self, collection_name: str):
        if self.client is None:
            self.connect()
        self.client.collections.delete(collection_name)

    def get_collection(self, collection_name: str):
        if self.client is None:
            self.connect()
        return self.client.collections.get(collection_name)

    def save_knowledge(self, knowledge_list: list):
        vector_creator = get_vector_creator()
        collection = self.get_collection("Knowledge")
        # Enter Weaviate context manager
        with collection.batch.dynamic() as batch:
            for knowledge_obj in knowledge_list:
                text = f"{knowledge_obj['title']}\n{knowledge_obj['content']}"
                # print(text)
                vector = vector_creator.create_vector(text)

                # Add object (including vector) to batch queue
                batch.add_object(
                    properties=knowledge_obj,
                    uuid=generate_uuid5(knowledge_obj['node_id']),
                    vector=vector
                    # references=reference_obj  # You can add references here
                )
        if len(collection.batch.failed_objects) > 0:
            error_message = f"Failed to import {len(collection.batch.failed_objects)} objects\n"
            for failed_object in collection.batch.failed_objects:
                error_message += f"{failed_object}\n"
            raise Exception(error_message)

    def search(self, query):
        query_vector = get_vector_creator().create_vector(query)
        if self.client is None:
            self.connect()

        knowledge_coll = self.client.collections.get("Knowledge")
        # Query Weaviate
        response = knowledge_coll.query.near_vector(
            near_vector=query_vector,
            limit=5,
            return_metadata=wq.MetadataQuery(distance=True),
        )
        return response

    def disconnect(self):
        if self.client is not None:
            self.client.close()


# Factory method to get VectorDatabase
def get_vector_database() -> VectorDatabase:
    return WeaviateVectorDatabase()

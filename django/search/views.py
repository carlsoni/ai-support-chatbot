from django.http import JsonResponse
from django.shortcuts import render
from .utils.tech_support_assistant import TechSupportAssistant


def index(request):
    return render(request, 'index.html')


def search(request):
    query = request.GET.get('q', '')
    results = TechSupportAssistant().generate_response(query)
    return JsonResponse({
        'results':  results,
    })

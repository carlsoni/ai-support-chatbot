$(document).ready(function () {
    $('#search-form').on('submit', function (event) {
        event.preventDefault();

        $('#results').empty().append(
            '<div class="spinner-border text-primary" role="status">' +
            '  <span class="visually-hidden">Loading...</span>' +
            '</div>'
        );

        $.ajax({
            url: '/search/',
            data: {
                'q': $('#search').val()
            },
            dataType: 'json',
            success: function (data) {
                $('#results').empty().append(
                    '<p>'
                    + data.results
                    + '</p>'
                );
            }
        });
    });
});
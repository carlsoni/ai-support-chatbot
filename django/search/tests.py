import json
from unittest.mock import patch

from django.test import TestCase, RequestFactory
from django.urls import reverse

from .views import search


class AcceptanceTest(TestCase):

    def test_home_page(self):
        response = self.client.get(reverse('index'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'index.html')

    @patch('search.views.Knowledge')
    def test_search(self, mock_knowledge):
        mock_knowledge.objects.all().values.return_value = [
            {'id': 1, 'title': 'First FAQ'},
            {'id': 2, 'title': 'Another FAQ'}
        ]

        factory = RequestFactory()
        request = factory.get(reverse('search'), {'q': 'test_query'})
        response = search(request)

        self.assertEqual(response.status_code, 200)

        data = json.loads(response.content)
        self.assertEqual(data['results'], [
            {'id': 1, 'title': 'First FAQ'},
            {'id': 2, 'title': 'Another FAQ'}
        ])

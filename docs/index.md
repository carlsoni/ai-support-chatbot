AI Search Proof of Concept Documentation
---

This is a proof of concept for an AI Tech Support Assistant using the 
Retrieval-augmented generation (RAG) technique using our IT Knowledge Base.


Basic design:

- Knowledge Base is extracted from Drupal 7 relational database
  - MariaDB with a Drupal 7 database backup from Acquia
- Knowledge is vectorized 
  - Currently, using https://huggingface.co/WhereIsAI/UAE-Large-V1 locally
  - Could use other models.
- Knowledge is stored in a vector database
  - Using Weaviate
- Web interface for searching
  - Using Django
- Generation of responses
  - Currently, using OpenAI's GPT-3
    - Query is vectorized and used to retrieve similar knowledge from vector database
    - The knowledge is used to provide context for prompt to GPT-3
    - GPT-3 generates a response
    - The response is returned to the Web interface


# Prerequisites

- Docker
- git
- SQL database backup of Drupal 7 IT site
  - can be downloaded from [Acquia Clouds UI](https://cloud.acquia.com/app/develop/applications)
- OpenAI API key


# Installation

```bash
git clone {this repo}
cd {this repo}
cp django/.env.example django/.env
# edit .env and set the OpenAI API key
docker compose build
docker compose up -d
# import the Drupal 7 database backup to the MariaDB container drupal database
#   TODO: explain how to do this or automate
docker compose exec web python manage.py create_vector_database_knowledge
docker compose exec web python manage.py embed_knowledge_to_vector_database
```

## Search Interface
http://127.0.0.1:8000/


# Development

See: [Development](development.md)



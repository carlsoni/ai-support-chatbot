Development Documentation
---

# TODO - work in progress

At the moment this document is just a scratch pad for development notes.

---

## Next Step

I think we need to break the Drupal knowledge base articles into smaller 
chunks and vectorize each chunk.
The idea is each article contains too much data negatively impacting the 
vectorization results.

---

## Adding Python Packages

add to `requirements.txt` and rebuild the docker image

## Rebuild the Knowledge Vector Database

```bash
docker compose exec web python manage.py delete_vector_database_knowledge
docker compose exec web python manage.py create_vector_database_knowledge
docker compose exec web python manage.py embed_knowledge_to_vector_database
```

---

## Django database not currently used for anything.
```bash
docker compose exec web python manage.py migrate
sudo chown $(id -u):$(id -g) ./*
docker compose build
docker compose up -d
docker compose down

```

- update `search/models.py`
- verify `search.apps.SearchConfig` in `aiweb/settings.py`
- `docker compose exec web python manage.py makemigrations search`
- `docker compose exec web python manage.py migrate`

---

References:
- https://docs.djangoproject.com/en/5.0/intro/install/
- https://github.com/docker/awesome-compose/tree/master/official-documentation-samples/django/
- https://hub.docker.com/_/mongo
- https://www.mongodb.com/compatibility/mongodb-and-django
- https://hub.docker.com/_/mongo-express
